import GoogleMapsLoader from 'google-maps'

GoogleMapsLoader.KEY = 'AIzaSyC7C-vZ1a3jEGOQm1cQZgWsgL6SlHFk8VY'

const DistanceMatrix = (oLat, oLng, destinations, callback) => {
  GoogleMapsLoader.load((google) => {
    let origin = new google.maps.LatLng(oLat, oLng)
    let destinationsList = []
    destinations.forEach(place => {
      destinationsList.push(new google.maps.LatLng(place.lat, place.lng))
    })
    const service = new google.maps.DistanceMatrixService()
    service.getDistanceMatrix({
      origins: [origin],
      destinations: destinations,
      travelMode: 'WALKING'
    }, callback)
  })
}

export default DistanceMatrix

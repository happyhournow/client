const getLocation = () => {
  return new Promise((resolve, reject) => {
    if (!navigator.geolocation) {
      const err = new Error('Sorry, your device does not support geolocation. Come on now.')
      reject(err)
    } else {
      navigator.geolocation.getCurrentPosition((pos) => {
        resolve(pos)
      }, (err) => {
        reject(err.message)
      })
    }
  })
}

export default getLocation

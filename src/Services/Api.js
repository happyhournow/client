import axios from 'axios'
import token from './Token'

const instance = axios.create({
  baseURL: 'http://172.20.10.4:9021'
})

const getPlaces = () => {
  return new Promise((resolve, reject) => {
    instance.get('/places')
    .then((res) => {
      resolve(res)
    }).catch((err) => {
      reject(err)
    })
  })
}

const getPlacesWithDistance = (unit, mode, maxDistance, lng, lat) => {
  return new Promise((resolve, reject) => {
    instance.get(`/places?unit=${unit}&mode=${mode}&maxdistance=${maxDistance}&lng=${lng}&lat=${lat}`)
    .then((res) => {
      resolve(res)
    })
    .catch((e) => {
      reject(e)
    })
  })
}

const getPlaceByID = (id, unit, mode, lat, lng) => {
  return new Promise((resolve, reject) => {
    instance.get(`places/${id}?unit=${unit}&mode=${mode}&lat=${lat}&lng=${lng}`, {
      headers: { 'x-auth': token() }
    })
    .then((res) => {
      resolve(res)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

export const User = {
  async createUser (name, email, password) {
    try {
      const result = await instance.post('/user', { name, email, password })
      return result
    } catch (e) {
      throw e
    }
  },
  async logInUser (email, password) {
    try {
      const result = await instance.post('/user/session', {
        email, password
      })
      return result
    } catch (e) {
      throw e
    }
  }
}

export default { getPlaces, getPlaceByID, getPlacesWithDistance }

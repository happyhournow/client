export default {
  data () {
    return {
      get token () {
        return localStorage.getItem('token') || 0
      },
      set token (value) {
        localStorage.setItem('token', value)
      }
    }
  }
}

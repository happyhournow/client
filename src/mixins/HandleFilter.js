export default {
  methods: {
    handleSearch () {
      this.$store.commit('setFilterEnabled', !this.$store.state.filterEnabled)
    }
  }
}

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Views/Home'
import Place from '@/components/Views/Place'
import Settings from '@/components/Views/Settings'
import LogIn from '@/components/Views/LogIn'
import CreateUser from '@/components/CreateUser/CreateUser'
import SubmitPlace from '@/components/Views/SubmitPlace'

Vue.use(Router)

export default new Router({
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/place/:id',
      name: 'Place',
      component: Place
    },
    {
      path: '/submit',
      name: 'Submit',
      component: SubmitPlace
    },
    {
      path: '/user/signup',
      name: 'SignUp',
      component: CreateUser
    },
    {
      path: '/user/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/user/login',
      name: 'Login',
      component: LogIn
    }
  ]
})

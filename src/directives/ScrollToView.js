export default {
  inserted (el, binding, vnode) {
    if (binding.value) {
      el.scrollIntoView({behavior: 'smooth', block: 'end', inline: 'center'})
    }
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import Seed from './seed'

Vue.use(Vuex)

let seed = new Seed()

export default new Vuex.Store({
  state: {
    day: seed.currentDay(),
    currentStartTime: seed.startTime(),
    currentEndTime: seed.endTime(),
    filterEnabled: false,
    currentRoute: 'home',
    userLocation: { lat: undefined, lng: undefined },
    destinationLocation: { lat: 0, lng: 0 },
    isMapActive: false,
    mapPicker: false,
    phone: null,
    locationEnabled: false,
    auth: false,
    dialogue: { open: false, message: 'n/a' }
  },
  getters: {
    day: state => { return state.day },
    startTime: state => { return state.currentStartTime },
    endTime: state => { return state.currentEndTime },
    filterEnabled: state => { return state.filterEnabled },
    currentRoute: state => { return state.currentRoute },
    userLocation: state => { return state.userLocation },
    destinationLocation: state => { return state.destinationLocation },
    isMapActive: state => { return state.isMapActive },
    mapPicker: state => { return state.mapPicker },
    phone: state => { return state.phone },
    locationEnabled: state => { return state.locationEnabled },
    auth: state => { return state.auth }
  },
  mutations: {
    setDay (state, newDay) {
      state.day = newDay
    },
    setStartTime (state, newStartTime) {
      state.currentStartTime = newStartTime
    },
    setEndTime (state, newEndTime) {
      state.currentEndTime = newEndTime
    },
    setFilterEnabled (state, newValue) {
      state.filterEnabled = newValue
    },
    setCurrentRoute (state, newValue) {
      state.currentRoute = newValue
    },
    setUserLocation (state, newValue) {
      state.userLocation = newValue
    },
    setDestinationLocation (state, newValue) {
      state.destinationLocation = newValue
    },
    setMapActive (state, newValue) {
      state.isMapActive = newValue
    },
    setMapPicker (state, newValue) {
      state.mapPicker = newValue
    },
    setPhone (state, newValue) {
      state.phone = newValue
    },
    setLocationEnabled (state, newValue) {
      state.locationEnabled = newValue
    },
    setAuth (state, newValue) {
      state.auth = newValue
    },
    setDialogue (state, newValue) {
      state.dialogue = newValue
    }
  }
})

import moment from 'moment'

export default class Seed {
  currentDay () {
    return {
      text: moment().format('dddd'),
      value: moment().day(),
      active: true
    }
  }
  startTime () {
    return {
      value: parseInt(moment().hours()),
      text: moment().format('ha'),
      active: true
    }
  }
  endTime () {
    return {
      value: parseInt(moment().add(2, 'h').format('HH')),
      text: moment().add(2, 'h').format('ha'),
      active: true
    }
  }
}
